import requests_html, re
from requests_html import HTMLSession

RechercheMaitre = "https://candidat.pole-emploi.fr/offres/recherche?motsCles=python&offresPartenaires=false&rayon=10&tri=1"

ListeRefOffres = set()

def InitSession():
    NbOffresSession = HTMLSession().get(RechercheMaitre).html.find("#zoneAfficherListeOffres > h1")[0].text
    NbOffresSession = int(re.match("\d+", NbOffresSession).group(0))

    if NbOffresSession % 150 == 0:
        NbPagesSession = NbOffresSession // 150
    else:
        NbPagesSession = (NbOffresSession // 150) + 1

    print(NbOffresSession, NbPagesSession)
    return NbPagesSession

def Session(NbPagesSession):
    for page in range(NbPagesSession):
    RechercheOffres = HTMLSession().get(RechercheMaitre + "&range=0-150")

    # Page d'offre >> https://candidat.pole-emploi.fr/offres/recherche/detail/099LRZD


    RefOffres = RechercheOffres.html.find("li.result")

    for ref in RefOffres:
        ListeRefOffres.add(ref.attrs["data-id-offre"])

    print(len(ListeRefOffres))

InitSession()
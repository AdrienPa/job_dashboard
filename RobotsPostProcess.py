""" Robot commun de post traitement des récoltes des robots de scrap """

""" Projet Job DashBoard : Groupe 2 
Module by Julien """

import re

def Analyse(ligne):
    perti_auto = 0

    if re.search("python", ligne["corps"], flags=re.I):
        ligne["python"] = True
        perti_auto += 1
    else:
        ligne["python"] = False

    if re.search("sql", ligne["corps"], flags=re.I):
        ligne["sql"] = True
        perti_auto += 1
    else:
        ligne["sql"] = False

    if re.search(" data| bigdata", ligne["corps"], flags=re.I):
        ligne["data"] = True
        perti_auto += 1
    else:
        ligne["data"] = False

    ligne["perti_auto"] = perti_auto

    return ligne
